package fr.froz.AriaCore;

import fr.froz.AriaCore.Menu.InventoryMenu;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.Socket;
import java.util.ArrayList;


public class Main extends JavaPlugin implements Listener{
    private void getPerm(Player p) throws Exception{
        String sentence;
        String modifiedSentence;
        BufferedReader inFromUser = new BufferedReader(new StringReader("perms%"+p.getName().toString()));
        Socket clientSocket = new Socket("localhost", 6789);
        DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        sentence = inFromUser.readLine();
        outToServer.writeBytes(sentence + '\n');
        modifiedSentence = inFromServer.readLine();
        String[] perm = modifiedSentence.split("%");
        clientSocket.close();
        ArrayList<String> toRemove = new ArrayList<>();
        ArrayList<String> toAdd = new ArrayList<>();
        p.setDisplayName(ChatColor.translateAlternateColorCodes('&',perm[0])+" "+p.getName());
        p.setPlayerListName(ChatColor.translateAlternateColorCodes('&',perm[0])+" "+p.getName());

        int i = 1;
        if(perm[i].equalsIgnoreCase("*")){
            p.setOp(true);
        }else {
            p.setOp(false);
            while (i < perm.length) {
                if (perm[i].contains("-")) {
                    toRemove.add(perm[i].replace("-", ""));
                } else {
                    toAdd.add(perm[i]);
                }
                i++;
                AriaAPI.addPermissions(p.getUniqueId(), toAdd, toRemove);
            }
        }
    }
    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args){
        if(command.getName().equalsIgnoreCase("hub")||command.getName().equalsIgnoreCase("lobby")){
            InventoryMenu menu = new InventoryMenu("Menu",9);
            menu.addButton("Gerard","lol",new ItemStack(Material.FISHING_ROD),1);
            if(sender instanceof Player){
                ((Player) sender).openInventory(menu.getInventory());
                sender.sendMessage(ChatColor.DARK_RED + "[AriaCore]" + ChatColor.WHITE + " Envoie au lobby...");
                //AriaAPI.lobby((Player) sender);
            } else {
                sender.sendMessage("[AriaCore] vous n'etes pas un joueur.");
            }

            return true;
        } else if(command.getName().equalsIgnoreCase("AriaMoney")){
            if(sender.hasPermission("AriaCraft.Admin")) {
                if (args[0].equalsIgnoreCase("add")) {
                    try {
                        sender.sendMessage(ChatColor.GREEN + "Argent : " + ChatColor.WHITE + String.valueOf(AriaAPI.addMoney(Bukkit.getPlayer(args[1]), args[3], Integer.valueOf(args[2]))));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (args[0].equalsIgnoreCase("get")) {
                    try {
                        sender.sendMessage(ChatColor.GREEN + "Argent : " + ChatColor.WHITE + String.valueOf(AriaAPI.getMoney(Bukkit.getPlayer(args[1]), args[2])));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                sender.sendMessage(ChatColor.RED +"[AriaCore] vous n'avez pas la permission.");
            }
            return true;
        } else if(command.getName().equalsIgnoreCase("grade")){
            if(sender.hasPermission("AriaCraft.SAdmin")) {
                try {
                    AriaAPI.setGrade(Bukkit.getPlayer(args[0]), args[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }else {
                sender.sendMessage(ChatColor.RED +"[AriaCore] vous n'avez pas la permission.");
            }
        }
        return false;
    }
    @Override
    public void onEnable(){
        Bukkit.getPluginManager().registerEvents(this,this);
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
    }
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e){
        try {
            getPerm(e.getPlayer());
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e){
        e.setFormat("%s: %s");
    }
}
